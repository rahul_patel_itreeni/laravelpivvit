<?php

namespace App\Http\Requests\Purcahse;

use Illuminate\Foundation\Http\FormRequest;

class CreatePurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$validation = array(
    		'customer_name' 	=> 'required|max:255',
    		'offering_id' 		=> 'required|exists:offerings,id',
    		'quantity'			=> 'required|numeric|min:1',	
    	);
        
    	return $validation;
    }
}
