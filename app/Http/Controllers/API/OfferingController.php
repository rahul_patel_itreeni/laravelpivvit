<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Offering;

class OfferingController extends Controller
{
	public function index(){
		return  Offering::get();
	}
}
