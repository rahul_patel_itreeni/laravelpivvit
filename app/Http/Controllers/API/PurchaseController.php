<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Purcahse\CreatePurchaseRequest;
use App\Models\Purchase;
use App\Models\Offering;

class PurchaseController extends Controller
{

	public function store(CreatePurchaseRequest $request){
		$input = $request->all();
		$challenge = Purchase::Create($input);
		
		if ($request->ajax()) {
	    	return response()->json([
	    		'success' => true,
	    		'message' => 'Purchase created successfully.',
	    	]);
	    }
	}
	
	public function index(){
		$purchases = Purchase::with(["offering"])->get();
		return $purchases; 
	}
}
