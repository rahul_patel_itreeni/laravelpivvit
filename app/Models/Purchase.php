<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	/**
	 * The attributes that are guared againsts mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
	/**
	 * Purchase has only one offer.
	 */
	public function offering(){
		return $this->belongsTo('App\Models\Offering');
	}
}
