<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offering extends Model
{
	/**
	 * The attributes that are guared againsts mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
	/**
	 * Offering can be associated with multiple purchases!
	 */
	public function purchases(){
		return $this->hasMany('App\Models\Purchase');
	}
}
