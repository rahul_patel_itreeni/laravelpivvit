<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get('/offerings', 'API\OfferingController@index')->name('api.offerings.index');
Route::post('/purchases', 'API\PurchaseController@store')->name('api.purchase.store');
Route::get('/purchases', 'API\PurchaseController@index')->name('api.purchase.index');
