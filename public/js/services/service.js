angular.module('PurchaseService', [])

.factory('Purchase', function($http) {

    return {
        get : function() {
            return $http.get('/purchases');
        },
        getOffering : function(){
        	return $http.get('/offerings');
        },
        save : function(purchaseData) {
            return $http({
                method: 'POST',
                url: '/purchases',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(purchaseData)
            })
        },
    }

});