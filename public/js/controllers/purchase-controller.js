// public/js/controllers/purchaseCtrl.js

angular.module('purchaseCtrl', [])

.controller('purchseController', function($scope, $http, Purchase) {
    $scope.purchaseData = {};
    $scope.offerings = {};
    $scope.loading = false;
    $scope.price = 0;
    $scope.purchaseData.quantity = 1;

    Purchase.get()
        .success(function(data) {
            $scope.purchases = data;
            $scope.loading = false;
        });

    Purchase.getOffering().success(function(data) {
        $scope.offerings = data;
        $scope.offering_id = $scope.offerings[0].id;
    });
    
    $scope.submitPurchase = function() {
        $scope.loading = true;

        // save the Purchase. pass in Purchase data from the form
        // use the function we created in our service
        Purchase.save($scope.purchaseData)
            .success(function(data) {
                // if successful, we'll need to refresh the Purchase list
                Purchase.get()
                    .success(function(getData) {
                        $scope.purchases = getData;
                        $scope.loading = false;
                    });

            })
            .error(function(data) {
            	 $scope.loading = false;
            	var errorMessage = ""; 
	            $.each(data, function(index, value){
	            	errorMessage += value + "\r\n";
	            });
	            if(errorMessage) {
	            	alert(errorMessage);
	            }
	           
            });
    };
    
    $scope.getPrice = function(){
    	$scope.price = 10;
    	
    }

});