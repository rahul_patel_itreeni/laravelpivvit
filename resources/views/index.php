<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Laravel and Angular System</title>

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script rc="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/bower_components/angular/angular.js"></script>
<script src="/js/controllers/purchase-controller.js"></script>
<script src="/js/services/service.js"></script>
<script src="/js/laravel-pivvit.js"></script>

</head>
<!-- declare our angular app and controller -->
<body class="container" ng-app="pivvitApp"
	ng-controller="purchseController">
	<div class="col-md-8 col-md-offset-2">

		<form class="form-horizontal" ng-submit="submitPurchase()">
			<div class="form-group">
				<label class="control-label col-sm-2" for="Select Offer">Select Offer:</label>
				<div class="col-sm-10">
					<select class="form-control" ng-model="purchaseData.offering_id" ng-change="getPrice()">
						<option ng-repeat="x in offerings" value="{{x.id}}">{{x.title}}</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="Customer Name">Customer Name:</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" ng-model="purchaseData.customer_name">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="Quantity">Quantity:</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" ng-model="purchaseData.quantity">
					<input class="form-control" type="hidden" ng-model="price">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="Total">Total:</label>
				<div class="col-sm-10">
					<label class="form-control"> {{price*purchaseData.quantity}} </label>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>

		<!-- LOADING ICON =============================================== -->
		<!-- show loading icon if the loading variable is set to true -->
		<p class="text-center" ng-show="loading">
			<span class="fa fa-meh-o fa-5x fa-spin"></span>
		</p>
	
		<div class="table-responsive" ng-if=purchases>
		  	<table class="table table-stripped">
		  		<thead>
		  			<th>Purchase ID</th>
		  			<th>Offering Title</th>
		  			<th>Quantity</th>
		  			<th>Unit Price</th>
		  			<th>Total</th>
		  		</thead>
		  		<tbody>
		  			<tr ng-repeat="purchase in purchases">
		  				<td>{{purchase.id}}</td>
		  				<td>{{purchase.offering.title}}</td>
		  				<td>{{purchase.quantity}}</td>
		  				<td>{{purchase.offering.price}}</td>
		  				<td>{{purchase.quantity * purchase.offering.price}}</td>
		  			</tr>
		  		</tbody>	
		  	</table>
		</div>	  	
	</div>
</body>
</html>